package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    public List<MedicoDto> listarTodos() {
        return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }

    public MedicoDto listarUno(Integer id) {
        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);
    }

    public MedicoDto agregar(MedicoDto entity) {
        entity.setId(null);
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));
    }
}
